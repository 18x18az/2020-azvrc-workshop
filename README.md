<a href="https://www.robotevents.com/robot-competitions/workshops/RE-WORKSHOP-20-1863.html" target="_blank"><img height="240" src="Thumbnail.png" /></a>

## Slides

https://docs.google.com/presentation/d/1I7kKnsOzJiLKJAn_KoBgSinu8TOTOtmsTBIanxR3V-o/edit?usp=sharing

## Recording

[https://youtu.be/BSicorGs8oY](https://youtu.be/BSicorGs8oY)

- [0:08:29 — Welcome](https://youtu.be/BSicorGs8oY?t=509)
- [0:13:02 — Team organization](https://youtu.be/BSicorGs8oY?t=782)
- [0:42:51 — Designing a competitive robot](https://youtu.be/BSicorGs8oY?t=2571)
- [1:38:22 — Building tips](https://youtu.be/BSicorGs8oY?t=5902)
- [3:09:04 — Judging and engineering notebooks](https://youtu.be/BSicorGs8oY?t=11344)
- [3:54:18 — Scouting and best practices at competitions](https://youtu.be/BSicorGs8oY?t=14058)
- [4:48:42 — Programming and sensors](https://youtu.be/BSicorGs8oY?t=17322)

## Links Posted in Chat

Official Q&A: <https://www.robotevents.com/VRC/2020-2021/QA>

Online Challenges: <https://challenges.robotevents.com/>

VEXcode VR: <https://www.vexrobotics.com/vexcode-vr>

STEM Labs: <https://education.vex.com/>

VEX Knowledge Base: <https://kb.vex.com/hc/en-us>

VEX sample builds: <https://www.vexrobotics.com/download-search?refinementList%5Bdocument_types%5D%5B0%5D=Build%20Instructions&refinementList%5Bproduct_lines%5D%5B0%5D=VEX%20EDR>

Here is one example of a community-provided CAD library: <https://www.vexforum.com/t/vex-cad-fusion-360-parts-library-release-log/83263?u=barin>

Here is a video series on learning to use Autodesk Inventor with VEX: <https://www.vexforum.com/t/vex-applied-autodesk-inventor-training-series/19765?u=barin>

STEM Labs: <https://education.vex.com/edr/stem-labs/>

VEX Knowledge Base: <https://kb.vex.com/hc/en-us>

VEX curriculum: <https://curriculum.vexrobotics.com/curriculum.html>

VEX KB page on drivetrains: <https://kb.vex.com/hc/en-us/articles/360035952771-Drivetrain-Drivetrains-for-VEX-V5>

VEX KB page on robot arms: <https://kb.vex.com/hc/en-us/articles/360037389012-Robot-Arms-Mechanisms-for-VEX-V5>

Pre-made stickers to help label wires: <https://www.robosource.net/42-stickers>

Great source of legal non-VEX parts: <https://www.robosource.net/>

Where to find registered teams: <https://www.robotevents.com/map>

Current Judge Guide: <http://www.roboticseducation.org/documents/2019/08/judge-guide.pdf>

Judging rubrics: <https://www.roboticseducation.org/documents/2020/06/engineering-notebook-and-team-interview-rubric.pdf/>

The judges at Worlds found this to be an excellent notebook: <https://www.roboticseducation.org/documents/2019/09/vrc-sample-engineering-notebook-team-1575a.pdf/>

Code of Conduct: <https://www.roboticseducation.org/documents/2019/08/recf-code-of-conduct.pdf/>

Skills Stop Time with Event Brain: <https://kb.vex.com/hc/en-us/articles/360044865591-Using-a-V5-Robot-Brain-for-Robot-Skills-Challenge-Field-Control>

Full rules for Skills (game manual): <https://content.vexrobotics.com/docs/vrc-change-up/Appendix-B-20200615.pdf>

Game Manual: <https://link.vex.com/docs/vrc-change-up/GameManual>

VRC Hub app: <https://www.vexrobotics.com/v5/competition/vrc-hub>

Official Q&A (repost): <https://www.robotevents.com/VRC/2020-2021/QA>

A few fundraising resources from the RECF: <https://www.roboticseducation.org/fundraising-resources/>

Grants provided in partnership with the RECF: <https://www.roboticseducation.org/grants/>

ESD notice for VRC teams (relates to dead V5 motors/ports): <https://kb.vex.com/hc/en-us/articles/360035589572-VRC-Team-ESD-Notice-Competition-Robots-for-VEX-V5>

VEXcode: <https://www.vexrobotics.com/vexcode>

PROS: <https://pros.cs.purdue.edu/>

Robot Mesh Studio: <https://www.robotmesh.com/studio>

MATLAB & Simulink (less commonly used programming option): <https://www.mathworks.com/academia/student-competitions/vex-robotics.html>

STEM Labs: <https://education.vex.com/edr/stem-labs/>

VEX V5 Knowledge Base: <https://kb.vex.com/hc/en-us/categories/360002333191-V5>

Robot Mesh Python introduction: <https://www.youtube.com/playlist?list=PLQu7Tvt7G2Lo_Vmya4XgkV-rgX6bgGPy_>

Robot Mesh Virtual Academy: <https://www.robotmesh.com/virtual-academy>

Great guide on PID control, frequently shared in the VEX community: <http://georgegillard.com/programming-guides/introduction_to_pid_controllers_ed2-pdf?format=raw>

Getting started with GitHub: <https://guides.github.com/activities/hello-world/>
